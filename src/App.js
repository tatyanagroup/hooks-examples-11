import React, { useState } from "react";
import searchIcon from "./images/search.svg";
import "./App.css";

const listOfStatuses = {
  SENT: "Sent",
  DRAFT: "Draft",
  SPAM: "Spam",
  BIN: "Bin",
};

function randomNumber(min, max) {
  return Math.floor(Math.random() * (max - min) + min);
}

function generateList() {
  return Array.from({ length: randomNumber(50, 100) }).map((i, indx) => ({
    name: `name ${indx}`,
    status: listOfStatuses[Object.keys(listOfStatuses)[randomNumber(0, 3)]],
    text: `random text ${indx}`,
  }));
}

const randomMessages = generateList();

function App() {
  const [filters, setFilters] = useState({
    sent: false,
    draft: false,
    spam: false,
    bin: false,
  });

  const messageList = randomMessages.filter((item) => {
    const keys = Object.values(listOfStatuses);
    const { sent, draft, spam, bin } = filters;
    const activeFilters = [sent, draft, spam, bin]
      .map((i, idx) => (i ? keys[idx] : null))
      .filter((i) => i);

    return activeFilters.length ? activeFilters.includes(item.status) : true;
  });

  return (
    <div>
      <div>
        <div className="content">
          <div className="sidebar">
            <nav className="menu">
              <div className="checkbox_wrapper">
                <div className="switch_box box_1">
                  <input
                    type="checkbox"
                    className="switch_1"
                    checked={filters.sent}
                    onChange={() =>
                      setFilters((prevState) => ({
                        ...prevState,
                        sent: !prevState.sent,
                      }))
                    }
                  />
                </div>
                <span className="checkbox_label">Sent</span>
              </div>

              <div href="#" className="checkbox_wrapper">
                <div className="switch_box box_1">
                  <input
                    type="checkbox"
                    className="switch_1"
                    checked={filters.draft}
                    onChange={() =>
                      setFilters((prevState) => ({
                        ...prevState,
                        draft: !prevState.draft,
                      }))
                    }
                  />
                </div>
                <span className="checkbox_label">Draft</span>
              </div>
              <div href="#" className="checkbox_wrapper">
                <div className="switch_box box_1">
                  <input
                    type="checkbox"
                    className="switch_1"
                    checked={filters.spam}
                    onChange={() =>
                      setFilters((prevState) => ({
                        ...prevState,
                        spam: !prevState.spam,
                      }))
                    }
                  />
                </div>
                <span className="checkbox_label">Spam</span>
              </div>
              <div href="#" className="checkbox_wrapper">
                <div className="switch_box box_1">
                  <input
                    type="checkbox"
                    className="switch_1"
                    checked={filters.bin}
                    onChange={() =>
                      setFilters((prevState) => ({
                        ...prevState,
                        bin: !prevState.bin,
                      }))
                    }
                  />
                </div>
                <span className="checkbox_label">Bin</span>
              </div>
              <hr className="mt-2" />

              <div className="checkbox_wrapper">
                <span className="checkbox_label">Labels</span>
              </div>
            </nav>
          </div>

          <div className="main-content">
            <header className="bottom-header">
              <div className="search-wrapper">
                <div className="search">
                  <div className="input-search">
                    <div className="input-search-container">
                      <span className="search-btn-wrapper">
                        <button type="submit" className="search-btn">
                          <img src={searchIcon} width="22" height="22" />
                        </button>
                      </span>
                      <input
                        type="search"
                        name="q"
                        className="search-input"
                        placeholder="Search..."
                        autocomplete="off"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </header>

            <main className="items-container">
              {messageList.map((item) => {
                return (
                  <div className="item-container">
                    <div className="item-box">
                      <div className="image-box">
                        <img
                          className="user-image"
                          src="https://images.unsplash.com/photo-1500648767791-00dcc994a43e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2.25&w=256&h=256&q=80"
                          alt=""
                        />

                        <div className="user-name">
                          <span>{item.name}</span>
                        </div>
                      </div>

                      <div className="text-container">
                        <div className="btn">
                          <span className="status-btn">{item.status}</span>
                        </div>

                        <div className="line-text">
                          <p>{item.text}</p>
                        </div>
                      </div>
                    </div>
                  </div>
                );
              })}
            </main>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
